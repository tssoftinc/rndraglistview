import React, { PropTypes, Component } from 'react';
import ReactNative, { requireNativeComponent, View, StyleSheet, UIManager } from 'react-native';

const iface = {
	name: 'DragScrollView',
	propTypes: {
		onContentSizeChange: PropTypes.func,
		draggedViewOpacity: PropTypes.number,
		...View.propTypes
	}
};

const RawDragScrollView = requireNativeComponent('DragScrollView', iface);

const createHandler = (handler, extractor) => {
	return handler ? e => handler(extractor(e)) : null;
}

class DragScrollView extends Component { 

	constructor(props) {
		super(props);
	}
	scrollTo(obj) {
		const {x, y, animated} = obj;
		UIManager.dispatchViewManagerCommand(
			ReactNative.findNodeHandle(this.refs.nativeView),
			UIManager.RCTScrollView.Commands.scrollTo,
			[x || 0, y || 0, animated !== false],
		);
	}
	
	scrollBy(y, animated) {
		UIManager.dispatchViewManagerCommand(
			ReactNative.findNodeHandle(this.refs.nativeView),
			100,
			[y || 0, animated !== false],
		);
	}
	
	render() {
		const {
			style,
			onScroll,
			onDragStart,
			onDragEnd,
			...props
		} = this.props;
		
		const wrappedChildren = React.Children.map(this.props.children, (child) => {
			if (!child) {
				return null;
			}
			return (
				<View collapsable={false} style={styles.absolute}>
					{child}
				</View>
			);
		});
		
		const onItemMovedHandler = createHandler(this.props.onRowMoved, event => event.nativeEvent);
		const onScrollHandler = createHandler(onScroll, event => event.nativeEvent);
		const onDragStartHandler = createHandler(onDragStart, event => event.nativeEvent.position);
		const onDragEndHandler = createHandler(onDragEnd, event => event.nativeEvent.position);
		const innerStyle = [{flex: 1}, style];

		return (
			<RawDragScrollView ref='nativeView' onScroll={onScrollHandler} onItemMoved={onItemMovedHandler} onDragStart={onDragStartHandler} onDragEnd={onDragEndHandler} style={innerStyle} {...props}>
				{wrappedChildren}
			</RawDragScrollView>
		);
	}
};

const styles = StyleSheet.create({
	absolute: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0
	}
});

export default DragScrollView;
