package com.tssoft.draglistview;

// this whole thing was copy-pasted from (private nested for some reason) com.facebook.react.views.recyclerview.RecyclerViewBackedScrollView.ScrollOffsetTracker (v. 0.30)
public class DragScrollOffsetTracker {
    private final DragScrollViewAdapter mAdapter;

    private int mLastRequestedPosition;
    private int mOffsetForLastPosition;

    public DragScrollOffsetTracker(DragScrollViewAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    public void onHeightChange(int index, int oldHeight, int newHeight) {
        if (index < mLastRequestedPosition) {
            mOffsetForLastPosition = (mOffsetForLastPosition - oldHeight + newHeight);
        }
    }

    public int getTopOffsetForItem(int index) {
        if (mLastRequestedPosition != index) {
            int sum;

            if (mLastRequestedPosition < index) {
                int startIndex;

                if (mLastRequestedPosition != -1) {
                    sum = mOffsetForLastPosition;
                    startIndex = mLastRequestedPosition;
                } else {
                    sum = 0;
                    startIndex = 0;
                }

                for (int i = startIndex; i < index; i++) {
                    sum += mAdapter.getView(i).getMeasuredHeight();
                }
            } else {
                if (index < (mLastRequestedPosition - index)) {
                    sum = 0;
                    for (int i = 0; i < index; i++) {
                        sum += mAdapter.getView(i).getMeasuredHeight();
                    }
                } else {
                    sum = mOffsetForLastPosition;
                    for (int i = mLastRequestedPosition - 1; i >= index; i--) {
                        sum -= mAdapter.getView(i).getMeasuredHeight();
                    }
                }
            }
            mLastRequestedPosition = index;
            mOffsetForLastPosition = sum;
        }
        return mOffsetForLastPosition;
    }
}
