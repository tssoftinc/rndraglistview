package com.tssoft.draglistview;

public interface DragAdapter {
    void onItemMove(int fromPosition, int toPosition);

    void onItemDragStart(int position);

    void onItemDragEnd(int position);
}
