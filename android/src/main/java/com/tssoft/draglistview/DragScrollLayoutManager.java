package com.tssoft.draglistview;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class DragScrollLayoutManager extends LinearLayoutManager {

    private boolean mDragging;

    public DragScrollLayoutManager(Context context) {
        super(context);
    }

    public void setDragging(boolean mDragging) {
        this.mDragging = mDragging;
    }

    @Override
    public void removeAndRecycleViewAt(int index, RecyclerView.Recycler recycler) {
        if (!mDragging) {
            super.removeAndRecycleViewAt(index, recycler);
        }
    }
}
