package com.tssoft.draglistview;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class DragScrollTouchCallback extends ItemTouchHelper.Callback {

    private DragAdapter mAdapter;

    private RecyclerView.ViewHolder mDraggedHolder;
    private boolean mWasDragged;

    public DragScrollTouchCallback(DragAdapter adapter) {
        this.mAdapter = adapter;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        int fromPosition = viewHolder.getAdapterPosition();
        int toPosition = target.getAdapterPosition();
        mAdapter.onItemMove(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (isCurrentlyActive && !mWasDragged && actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            mWasDragged = true;
            mAdapter.onItemDragStart(viewHolder.getAdapterPosition());
            DragScrollView mainView = (DragScrollView)recyclerView;
            viewHolder.itemView.setAlpha(mainView.getDraggedViewOpacity());
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder == null && mDraggedHolder != null && mWasDragged) {
            mAdapter.onItemDragEnd(mDraggedHolder.getAdapterPosition());
            mDraggedHolder.itemView.setAlpha(1.0f);
        }
        mDraggedHolder = viewHolder;
        mWasDragged = false;
    }
}
