package com.tssoft.draglistview;

import android.content.Context;
import android.os.Debug;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DragScrollViewAdapter extends RecyclerView.Adapter<DragScrollViewAdapter.ViewContainerHolder> implements DragAdapter {

    private final List<View> mViews = new ArrayList<>();
    private final DragScrollView mMainView;
    private final DragScrollOffsetTracker mOffsetTracker;

    private int mTotalChildrenHeight = 0;

    private final View.OnLayoutChangeListener mChildLayoutChangeListener = new View.OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

            int oldHeight = (oldBottom - oldTop);
            int newHeight = (bottom - top);

            if (oldHeight != newHeight) {
                updateTotalChildrenHeight(newHeight - oldHeight);
                mOffsetTracker.onHeightChange(mViews.indexOf(v), oldHeight, newHeight);
                if (v.getParent() != null && v.getParent().getParent() != null) {
                    View wrapper = (View) v.getParent();
                    wrapper.forceLayout();
                    ((View) wrapper.getParent()).forceLayout();
                }
            }
        }
    };

    public DragScrollViewAdapter(DragScrollView view) {
        mMainView = view;
        mOffsetTracker = new DragScrollOffsetTracker(this);
        setHasStableIds(true);
    }

    @Override
    public ViewContainerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewContainer container = new ViewContainer(parent.getContext());
        ViewContainerHolder holder = new ViewContainerHolder(container);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewContainerHolder holder, int position) {
        View view = mViews.get(position);
        ViewGroup oldParent = (ViewGroup)view.getParent();
        ViewContainer newParent = holder.getContainer();
        if (oldParent != newParent) {
            if (oldParent != null) {
                oldParent.removeView(view);
            }
            newParent.addView(view);
        }
    }

    @Override
    public void onViewRecycled(ViewContainerHolder holder) {
        super.onViewRecycled(holder);
        holder.getContainer().removeAllViews();
    }

    @Override
    public int getItemCount() {
        return mViews.size();
    }

    @Override
    public long getItemId(int position) {
        return mViews.get(position).getId();
    }

    @Override
    public synchronized void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }
        View moved = mViews.get(fromPosition);
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; ++i) {
                mViews.set(i, mViews.get(i + 1));
            }
        } else {
            for (int i = fromPosition; i > toPosition; --i) {
                mViews.set(i, mViews.get(i - 1));
            }
        }
        mViews.set(toPosition, moved);

        notifyItemMoved(fromPosition, toPosition);
        notifyJs(fromPosition, toPosition);
    }

    private ReactContext getContext() {
        return (ReactContext)mMainView.getContext();
    }

    private RCTEventEmitter getEmitter() {
        return getContext().getJSModule(RCTEventEmitter.class);
    }

    private DragScrollLayoutManager getLayoutManager() {
        return ((DragScrollLayoutManager)mMainView.getLayoutManager());
    }

    @Override
    public void onItemDragEnd(int position) {
        getLayoutManager().setDragging(false);

        WritableMap event = Arguments.createMap();
        event.putInt("position", position);
        getEmitter().receiveEvent(
                mMainView.getId(),
                "dragEnd",
                event
        );
    }

    @Override
    public void onItemDragStart(int position) {
        getLayoutManager().setDragging(true);

        WritableMap event = Arguments.createMap();
        event.putInt("position", position);
        getEmitter().receiveEvent(
                mMainView.getId(),
                "dragStart",
                event
        );
    }

    private void notifyJs(int fromPosition, int toPosition) {
        WritableMap event = Arguments.createMap();
        event.putInt("from", fromPosition);
        event.putInt("to", toPosition);
        getEmitter().receiveEvent(
                mMainView.getId(),
                "itemMoved",
                event
        );
    }

    public void addView(View view, int index) {
        mViews.add(index, view);
        updateTotalChildrenHeight(view.getMeasuredHeight());
        view.addOnLayoutChangeListener(mChildLayoutChangeListener);
        notifyItemInserted(index);
    }

    public void removeViewAt(int index) {
        View view = mViews.get(index);
        if (view != null) {
            mViews.remove(index);
            view.removeOnLayoutChangeListener(mChildLayoutChangeListener);
            updateTotalChildrenHeight(-view.getMeasuredHeight());
            notifyItemRemoved(index);
        }
    }

    public View getView(int index) {
        return mViews.get(index);
    }

    public int getTopOffsetForItem(int index) {
        return mOffsetTracker.getTopOffsetForItem(index);
    }

    private void updateTotalChildrenHeight(int delta) {
        if (delta != 0) {
            mTotalChildrenHeight += delta;
            mMainView.onTotalChildrenHeightChange(mTotalChildrenHeight);
        }
    }

    public int getTotalChildrenHeight() {
        return mTotalChildrenHeight;
    }

    public static class ViewContainer extends ViewGroup {
        public ViewContainer(Context context) {
            super(context);
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            if (getChildCount() > 0) {
                View child = getChildAt(0);
                setMeasuredDimension(child.getMeasuredWidth(), child.getMeasuredHeight());
            }
        }
    }

    public static class ViewContainerHolder extends RecyclerView.ViewHolder {
        public ViewContainerHolder(ViewContainer itemView) {
            super(itemView);
        }

        public ViewContainer getContainer() {
            return (ViewContainer)itemView;
        }
    }
}
