package com.tssoft.draglistview;

import android.view.View;

import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.PixelUtil;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewGroupManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.views.recyclerview.ContentSizeChangeEvent;
import com.facebook.react.views.scroll.ReactScrollViewCommandHelper;
import com.facebook.react.views.scroll.ScrollEventType;

import java.util.Map;

import javax.annotation.Nullable;

public class DragScrollViewManager
        extends ViewGroupManager<DragScrollView>
        implements ReactScrollViewCommandHelper.ScrollCommandHandler<DragScrollView> {

    public static final String REACT_CLASS = "DragScrollView";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    protected DragScrollView createViewInstance(ThemedReactContext reactContext) {
        DragScrollView view = new DragScrollView(reactContext);
        return view;
    }

    @Override
    public void addView(DragScrollView parent, View child, int index) {
        parent.addViewToAdapter(child, index);
    }

    @Override
    public int getChildCount(DragScrollView parent) {
        return parent.getChildCountFromAdapter();
    }

    @Override
    public View getChildAt(DragScrollView parent, int index) {
        return parent.getChildAtFromAdapter(index);
    }

    @Override
    public void removeViewAt(DragScrollView parent, int index) {
        parent.removeViewFromAdapter(index);
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        Map<String, Object> events = MapBuilder.<String, Object>of(
                "itemMoved",
                MapBuilder.of("registrationName", "onItemMoved"),
                "dragStart",
                MapBuilder.of("registrationName", "onDragStart"),
                "dragEnd",
                MapBuilder.of("registrationName", "onDragEnd"),
                ScrollEventType.SCROLL.getJSEventName(),
                MapBuilder.of("registrationName", "onScroll"),
                ContentSizeChangeEvent.EVENT_NAME,
                MapBuilder.of("registrationName", "onContentSizeChange"));
        return events;
    }

    private static final int SCROLL_BY = 100;

    @Nullable
    @Override
    public Map<String, Integer> getCommandsMap() {
        Map<String, Integer> commands = ReactScrollViewCommandHelper.getCommandsMap();
        commands.put("scrollBy", SCROLL_BY);
        return commands;
    }

    @Override
    public void receiveCommand(
            DragScrollView scrollView,
            int commandId,
            @Nullable ReadableArray args) {
        if (commandId == SCROLL_BY) {
            if (args == null) {
                throw new RuntimeException("SCROLL_BY args is null");
            }
            int deltaY = Math.round(PixelUtil.toPixelFromDIP(args.getDouble(0)));
            boolean animated = args.getBoolean(1);
            scrollView.scrollBy(deltaY, animated);
        } else {
            ReactScrollViewCommandHelper.receiveCommand(this, scrollView, commandId, args);
        }
    }

    @Override
    public void scrollTo(DragScrollView scrollView, ReactScrollViewCommandHelper.ScrollToCommandData data) {
        scrollView.scrollTo(data.mDestX, data.mDestY, data.mAnimated);
    }

    @ReactProp(name = "onContentSizeChange")
    public void setOnContentSizeChange(DragScrollView view, boolean value) {
        view.setSendContentSizeChangeEvents(value);
    }

    @ReactProp(name = "draggedViewOpacity", defaultFloat = 1.0f)
    public void setDraggedViewOpacity(DragScrollView view, float value) {
        view.setDraggedViewOpacity(value);
    }
}
