package com.tssoft.draglistview;

import android.annotation.SuppressLint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.react.bridge.ReactContext;
import com.facebook.react.common.SystemClock;
import com.facebook.react.uimanager.UIManagerModule;
import com.facebook.react.views.recyclerview.ContentSizeChangeEvent;
import com.facebook.react.views.scroll.ScrollEvent;
import com.facebook.react.views.scroll.ScrollEventType;

import java.lang.reflect.Field;

public class DragScrollView extends RecyclerView {

    private final DragScrollViewAdapter mAdapter;

    private boolean mSendContentSizeChangeEvents;
    private float mDraggedViewOpacity;

    private static final Field sFirstLayoutComplete;
    private static final Field sLayoutRequestEaten;

    static {
        try {
            Class recyclerView = RecyclerView.class;
            sFirstLayoutComplete = recyclerView.getDeclaredField("mFirstLayoutComplete");
            sFirstLayoutComplete.setAccessible(true);
            sLayoutRequestEaten = recyclerView.getDeclaredField("mLayoutRequestEaten");
            sLayoutRequestEaten.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    public DragScrollView(ReactContext context) {
        super(context);

        mAdapter = new DragScrollViewAdapter(this);
        mDraggedViewOpacity = 1.0f;

        this.setLayoutManager(new DragScrollLayoutManager(getContext()));
        this.setHasFixedSize(true);
        this.setAdapter(mAdapter);

        DragScrollTouchCallback callback = new DragScrollTouchCallback(mAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(this);
    }

    public void addViewToAdapter(View child, int index) {
        mAdapter.addView(child, index);
    }

    public void removeViewFromAdapter(int index) {
        mAdapter.removeViewAt(index);
    }

    public View getChildAtFromAdapter(int index) {
        return mAdapter.getView(index);
    }

    public int getChildCountFromAdapter() {
        return getAdapter().getItemCount();
    }

    public float getDraggedViewOpacity() {
        return mDraggedViewOpacity;
    }

    public void setDraggedViewOpacity(float opacity) {
        if (mDraggedViewOpacity < 0.0f || mDraggedViewOpacity > 1.0f) {
            throw new IllegalArgumentException("opacity should be between 0.0 and 1.0");
        }
        mDraggedViewOpacity = opacity;
    }

    public void scrollTo(int x, int y, boolean animated) {
        int deltaY = y - calculateAbsoluteOffset();
        scrollBy(deltaY, animated);
    }

    public void scrollBy(int deltaY, boolean animated) {
        if (animated) {
            smoothScrollBy(0, deltaY);
        } else {
            scrollBy(0, deltaY);
        }
    }

    private int calculateAbsoluteOffset() {
        int offsetY = 0;
        if (getChildCount() > 0) {
            View firstVisibleChild = getChildAt(0);
            int firstVisibleChildIndex = getChildViewHolder(firstVisibleChild).getLayoutPosition();
            offsetY = mAdapter.getTopOffsetForItem(firstVisibleChildIndex) - firstVisibleChild.getTop();
        }
        return offsetY;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        ((ReactContext)getContext()).getNativeModule(UIManagerModule.class).getEventDispatcher()
                .dispatchEvent(ScrollEvent.obtain(
                        getId(),
                        SystemClock.nanoTime(),
                        ScrollEventType.SCROLL,
                        0,
                        calculateAbsoluteOffset(),
                        getWidth(),
                        mAdapter.getTotalChildrenHeight(),
                        getWidth(),
                        getHeight()));
    }


    public void setSendContentSizeChangeEvents(boolean sendContentSizeChangeEvents) {
        mSendContentSizeChangeEvents = sendContentSizeChangeEvents;
    }

    public void onTotalChildrenHeightChange(int newTotalChildrenHeight) {
        if (mSendContentSizeChangeEvents) {
            ((ReactContext)getContext()).getNativeModule(UIManagerModule.class).getEventDispatcher()
                    .dispatchEvent(new ContentSizeChangeEvent(
                            getId(),
                            SystemClock.nanoTime(),
                            getWidth(),
                            newTotalChildrenHeight));
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
		// HACK: we need to reset the private field 'mFirstLayoutComplete' to true
		// otherwise, as layout() is never called by React, touch events are not processed any more
        resetLayoutComplete();
    }

    @Override
    @SuppressLint("MissingSuperCall")
    public void requestLayout() {
		// HACK: we need to reset the private field 'mLayoutRequestEaten'
		// instead of calling super.requestLayout
		// otherwise, as layout() is never called by React, touch events are not processed any more
        setEaten();
    }

    private void resetLayoutComplete() {
        try {
            sFirstLayoutComplete.set(this, true);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void setEaten() {
        try {
            sLayoutRequestEaten.set(this, true);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
