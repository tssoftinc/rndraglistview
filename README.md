## Installation

* Install package from npm

```sh
npm install --save react-native-draglistview
```

* Link the package: add these lines to `android/settings.gradle`

```
include ':RNDragListView'                                                                                                  
project(':RNDragListView').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-draglistview/android')                        
```

* Link the package: add the following line to dependencies in `android/app/build.gradle`

```
compile project(':RNDragListView')
```

* Link the package: add the package to MainApplication:

```diff
...
+ import com.tssoft.draglistview.DragScrollViewPackage;                                                                                 
...
protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
+          new DragScrollViewPackage()                                                                                         
      );
    }
  };
...
```

## Usage example

```javascript
	<DragListView
		data={['item 1', 'item 2', 'item 3']}
		onRowMoved={event => console.log(event.from, event.to)}
		renderRow={row => <Text>{row}</Text>}
		/>
```