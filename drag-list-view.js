import React, { Component } from 'react';
import { View } from 'react-native';
import DragScrollView from './drag-scroll-view';

const withoutHeight = style => typeof style === 'undefined' || style === null || typeof style.height === 'undefined';

const getHeightStyle = height => ({height: height, maxHeight: height + 1});

class DragListView extends Component {
	constructor(props) {
		super(props);
		this._onRowMoved = this._onRowMoved.bind(this);
		this.order = Object.keys(props.data);
		this._childHeight = [];
		
		this.state = {};
	}
	
	componentWillReceiveProps(props) {
		if (this.props.data !== props.data) {
			this.order = Object.keys(props.data);
		}
	}
	
	_onRowMoved(event) {
		
		let moved = this.order[event.from];
		if (event.from < event.to) {
			for (let i = event.from; i < event.to; ++i) {
				this.order[i] = this.order[i+1];
			}
		} else if (event.from > event.to) {
			for (let i = event.from; i > event.to; --i) {
				this.order[i] = this.order[i-1];
			}
		}
		this.order[event.to] = moved;
		
		if (this.props.onRowMoved) {
			this.props.onRowMoved(event);
		}
	}
	
	orderedData() {
		return this.order.map(i => this.data[i]);
	}
	
	_onChildLayout(layout, index) {
		if (!this.props.inner) {
			return;
		}
		
		const height = layout.height;
		if (this._childHeight[index] !== height) {
			this._childHeight[index] = height;

			let childrenHeightSum = 0;
			for (let i = 0; i < this.props.data.length; ++i) {
				childrenHeightSum += this._childHeight[i];
			}
			if (!isNaN(childrenHeightSum) && this.state.childrenHeightSum !== childrenHeightSum) {
				this.setState({childrenHeightSum: childrenHeightSum});
			}
		}
	}
	
	render() {
		const {
			data,
			inner,
			onRowMoved,
			style,
			...props,
		} = this.props;

		const getStyle = style => {
			const isArray = Array.isArray(style);
			if (!this.props.inner || typeof(this.state.childrenHeightSum) === 'undefined') {
				return style;
			} else if (typeof style === 'undefined') {
				return getHeightStyle(this.state.childrenHeightSum);
			} else if (!isArray && withoutHeight(style)) {
				return [getHeightStyle(this.state.childrenHeightSum), style];
			} else if (isArray && style.every(withoutHeight)) {
				return [getHeightStyle(this.state.childrenHeightSum), ...style];
			} else {
				return style;
			}
		};

		const bodyComponents = data.map((elem, index) => {
			return (
				<View key={'r_' + index} onLayout={this.props.inner ? event => this._onChildLayout(event.nativeEvent.layout, index) : null}>
					{this.props.renderRow(elem, null, index)}
				</View>
			);
		});
		
		return (
			<DragScrollView ref='scrollView' onRowMoved={this._onRowMoved} style={getStyle(style)} {...props}>
				{bodyComponents}
			</DragScrollView>
		);
	}
};

export default DragListView;
